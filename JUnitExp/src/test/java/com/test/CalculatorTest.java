package com.test;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class CalculatorTest {
		
	Calculator obj;
	
	@BeforeClass
	public static void beforeClass()
	{
		System.out.println("before all test cases");
	}
	
	@AfterClass
	public static void afterClass()
	{
		System.out.println("after all test cases ");
	}
	
	@Before
	public void setUp()
	{
		System.out.println("before test method");
		
		obj = new Calculator();
	}
	
	@After
	public void setDown() 
	{
		System.out.println("after test cases");
		
		obj = null;
	}
	
	@Test
	public void testAdd()
	{
		System.out.println("test add method");
		
		assertEquals(300, obj.add(200, 100));
		
	}
	
	@Test
	public void testSub()
	{
		System.out.println("test sub method");
		
		assertEquals(50, obj.sub(100, 50));
		
	}
	
	@Test
	public void testMul()
	{
		System.out.println("test mul method");
		
		assertEquals(100, obj.mul(50, 2));
		
	}
	
	@Test
	public void testGreetUser()
	{
		System.out.println("test greet method");
		
		assertEquals("Hi hello.", obj.greetUser("Hi hello."));
		
	}
}
